var fs = require('fs')
var path = require('path')

module.exports = function(dir,ext,callback) {
  //  var callback_inner = function(err,data) {
   fs.readdir(dir, function doneReading(err, fileContents){
     if (err)
     {
        return callback(err)
     }
     list = fileContents.filter(function(x){
      //  console.log("PathExtension"+path.extname(x));
      //   console.log(ext);
       return path.extname(x) == '.'+ext;
       });
     callback(null,list);
   });
  //  };
}

//original solution
// solution_filter.js:
//
//     var fs = require('fs')
//     var path = require('path')
//
//     module.exports = function (dir, filterStr, callback) {
//
//       fs.readdir(dir, function (err, list) {
//         if (err)
//           return callback(err)
//
//         list = list.filter(function (file) {
//           return path.extname(file) === '.' + filterStr
//         })
//
//         callback(null, list)
//       })
//     }
