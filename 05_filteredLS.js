var fs = require('fs')
var path = require('path')
dirName = process.argv[2]

extName = '.'+process.argv[3]


function extFiles(callback) {
  fs.readdir(dirName, function doneReading(err, fileContents){
  list = fileContents.filter(function(x){return path.extname(x) == extName});
  callback();
  });
}

function logMyFiles(){
  for (i=0;i<list.length;i++)
  {
    console.log(list[i])
  }
}
extFiles(logMyFiles)

//orginal solution

// var fs = require('fs')
//     var path = require('path')
//
//     fs.readdir(process.argv[2], function (err, list) {
//       list.forEach(function (file) {
//         if (path.extname(file) === '.' + process.argv[3])
//           console.log(file)
//       })
//     })
