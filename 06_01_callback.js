var filterByExtension = require('./06_module_filter.js');

var responseHandler = function(err,response){
  if (err)
  {
    console.error(err);
  }

  for (i=0;i<response.length;i++)
  {
    console.log(response[i])
  }
}
filterByExtension(process.argv[2],process.argv[3],responseHandler);

//original solution
// solution.js:
//
//     var filterFn = require('./solution_filter.js')
//     var dir = process.argv[2]
//     var filterStr = process.argv[3]
//
//     filterFn(dir, filterStr, function (err, list) {
//       if (err)
//         return console.error('There was an error:', err)
//
//       list.forEach(function (file) {
//         console.log(file)
//       })
//     })
