var http = require('http');

var makeRequest = function(response) {
  var str = '';

//another chunk of data has been recieved, so append it to `str`
  response.on('data', function (chunk) {
  console.log(chunk.toString());
  });

//the whole response has been recieved, so we just print it out here
response.on('error', console.error);

}

var url = process.argv[2].toString();

http.get(url,makeRequest);

// original solution
//
// var http = require('http')
//
//    http.get(process.argv[2], function (response) {
//      response.setEncoding('utf8')
//      response.on('data', console.log)
//      response.on('error', console.error)
//    })
//
